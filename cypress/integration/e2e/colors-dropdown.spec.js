describe('Colors page', () => {
  before(() => {
    /* Visit the URL */
    cy.visit('https://suse.eosdesignsystem.com/colors')

    /* Wait for 1s for the popup to show up */
    cy.wait(1100)

    /* Close the popup */
    cy.get('.close > .js-tooltip > .eos-icons').click()
  })

  it('should have a dropdown element', () => {
    /* Clicks on the Colors assets dropdown */
    cy.contains('Colors assets').click()

    /* Checks for the multiple links that need to be part of the dropdown */
    cy.contains('a', 'Colors stylesheet')
    cy.contains('a', 'Adobe color palettes')
    cy.contains('a', 'Inkscape color palettes')
  })
})
