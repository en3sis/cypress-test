describe('Signin page', () => {

  before(() => cy.visit('https://suse.eosdesignsystem.com/examples/demo-sign-in.html'))

  context('Page structure', () => {
    it('should display all the buttons', () => {
      cy.contains('Language')
        .click()
      cy.contains('English')
      cy.contains('German')
      cy.contains('Language')
        .click()
      cy.contains('English').not('be.visible')
    })
  })

  context('Form', () => {
    it('should', () => {
      cy.contains('Sign in')
      cy.get("input[type = 'text']").type('Hey')
      cy.get("input[type = 'password']").type('hello world')
      cy.contains('Button', 'Submit')
    })
  })
})
